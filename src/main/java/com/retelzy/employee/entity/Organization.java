package com.retelzy.employee.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Organization {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orgId;
    @Column(unique=true)
    private Long userId;
	private String lastNameOfEmpOrAuthorizedRepresentative;
    private String firstNameOfEmpOrAuthorizedRepresentative;
    private String empBusinessOrOrgName;
    private String titleOfEmpOrAuthorizedRepresentative;
    private Date firstDayOfEmployment;
    private Date dateofRehire;
    private String streetNumberAndName;
    private Double pinCodeOrZipCode;
    private String townCity;
    private String state;
    private String country;
    private String salary;
    private String clientName;
    private String employmentLocation;
    private Date projectEndDate;
}
