package com.retelzy.employee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.retelzy.employee.entity.Address;
import com.retelzy.employee.entity.City;
import com.retelzy.employee.entity.Country;
import com.retelzy.employee.entity.Employee;
import com.retelzy.employee.entity.Organization;
import com.retelzy.employee.entity.State;
import com.retelzy.employee.model.AddressModel;
import com.retelzy.employee.model.EmployeeModel;
import com.retelzy.employee.model.OrganizationModel;
import com.retelzy.employee.repository.AddressRepository;
import com.retelzy.employee.repository.CityRepository;
import com.retelzy.employee.repository.CountryRepository;
import com.retelzy.employee.repository.EmployeeRepository;
import com.retelzy.employee.repository.OrganizationRepository;
import com.retelzy.employee.repository.StateRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private CityRepository cityRepository;
	
	@Override
	public Integer checkRecord(Long userId) {
		
		Integer record = employeeRepository.checkEmpRecord(userId);
		return record;
	}

	@Override
	public Employee saveEmployee(EmployeeModel employeeModel) {
		Employee employee = new Employee();
		
		employee.setUserId(employeeModel.getUserId());
		employee.setLastName(employeeModel.getLastName());
		employee.setFirstName(employeeModel.getFirstName());
		employee.setMiddleInitial(employeeModel.getMiddleInitial());
		employee.setOtherLastName(employeeModel.getOtherLastName());
		employee.setDateOfBirth(employeeModel.getDateOfBirth());
		employee.setGender(employeeModel.getGender());
		employee.setSsn(employeeModel.getSsn());
		employee.setEmail(employeeModel.getEmail());
		employee.setTelephoneNumber(employeeModel.getTelephoneNumber());
		employee.setMobileNumber(employeeModel.getMobileNumber());
		employee.setAlternateMobileNumber(employeeModel.getAlternateMobileNumber());
		employeeRepository.save(employee);
		return employee;
	}
	
	@Override
	public Integer updateEmployee(EmployeeModel employeeModel) {
		Employee e = new Employee();
		
		e.setUserId(employeeModel.getUserId());
		e.setLastName(employeeModel.getLastName());
		e.setFirstName(employeeModel.getFirstName());
		e.setMiddleInitial(employeeModel.getMiddleInitial());
		e.setOtherLastName(employeeModel.getOtherLastName());
		e.setDateOfBirth(employeeModel.getDateOfBirth());
		e.setGender(employeeModel.getGender());
		e.setSsn(employeeModel.getSsn());
		e.setEmail(employeeModel.getEmail());
		e.setTelephoneNumber(employeeModel.getTelephoneNumber());
		e.setMobileNumber(employeeModel.getMobileNumber());
		e.setAlternateMobileNumber(employeeModel.getAlternateMobileNumber());
		return employeeRepository.updateEmployee(e.getMiddleInitial(),e.getOtherLastName(),e.getDateOfBirth(),
				e.getGender(),e.getSsn(),e.getTelephoneNumber(),e.getMobileNumber(),e.getAlternateMobileNumber(),e.getUserId());
	}

	
	@Override
	public Address saveAddress(AddressModel addressModel) {
		Address address = new Address();
		
		address.setUserId(addressModel.getUserId());
		address.setAptNumber(addressModel.getAptNumber());
		address.setSreetNumberAndName(addressModel.getSreetNumberAndName());
		address.setPinCodeOrZipCode(addressModel.getPinCodeOrZipCode());
		address.setAreaLocality(addressModel.getAreaLocality());
		address.setTownCity(addressModel.getTownCity());
		address.setState(addressModel.getState());
		address.setCountry(addressModel.getCountry());
		addressRepository.save(address);
		return address;
	}
	
	@Override
	public Organization saveOrganization(OrganizationModel organizationModel) {
		Organization organization = new Organization();
		
		organization.setUserId(organizationModel.getUserId());
		organization.setLastNameOfEmpOrAuthorizedRepresentative(organizationModel.getLastNameOfEmpOrAuthorizedRepresentative());
		organization.setFirstNameOfEmpOrAuthorizedRepresentative(organizationModel.getFirstNameOfEmpOrAuthorizedRepresentative());
		organization.setEmpBusinessOrOrgName(organizationModel.getEmpBusinessOrOrgName());
		organization.setTitleOfEmpOrAuthorizedRepresentative(organizationModel.getTitleOfEmpOrAuthorizedRepresentative());
		organization.setFirstDayOfEmployment(organizationModel.getFirstDayOfEmployment());
		organization.setDateofRehire(organizationModel.getDateofRehire());
		organization.setStreetNumberAndName(organizationModel.getStreetNumberAndName());
		organization.setPinCodeOrZipCode(organizationModel.getPinCodeOrZipCode());
		organization.setTownCity(organizationModel.getTownCity());
		organization.setState(organizationModel.getState());
		organization.setCountry(organizationModel.getCountry());
		organization.setSalary(organizationModel.getSalary());
		organization.setClientName(organizationModel.getClientName());
		organization.setEmploymentLocation(organizationModel.getEmploymentLocation());
		organization.setProjectEndDate(organizationModel.getProjectEndDate());
		organizationRepository.save(organization);
		return organization;
	}

	@Override
	public Employee getEmployeeInfo(String email) {
		return employeeRepository.getEmployee(email);
		
	}
	
	@Override
	public Address getAddressInfo(String email) {
		return addressRepository.getAddress(email);
		
	}
	
	@Override
	public Organization getOrganizationInfo(Integer userId) {
		return organizationRepository.getOrganization(userId);
		
	}

	@Override
	public Integer checkRecordInAddress(Long userId) {
		Integer record = addressRepository.checkRecordInAddress(userId);
		return record;
	}

	@Override
	public Integer checkRecordInOrganization(Long userId) {
		Integer record = organizationRepository.checkRecordInOrganization(userId);
		return record;
	}

	@Override
	public Integer updateAddress(AddressModel addressModel) {
		Address a = new Address();
		a.setUserId(addressModel.getUserId());
		a.setAptNumber(addressModel.getAptNumber());
		a.setSreetNumberAndName(addressModel.getSreetNumberAndName());
		a.setPinCodeOrZipCode(addressModel.getPinCodeOrZipCode());
		a.setAreaLocality(addressModel.getAreaLocality());
		a.setTownCity(addressModel.getTownCity());
		a.setState(addressModel.getState());
		a.setCountry(addressModel.getCountry());
		return addressRepository.updateAddress(a.getAptNumber(), a.getSreetNumberAndName(), a.getPinCodeOrZipCode(),a.getAreaLocality(),
				a.getTownCity(),a.getState(),a.getCountry(),a.getUserId());
	}

	@Override
	public Integer updateOrganization(OrganizationModel organizationModel) {
		Organization o = new Organization();
		o.setUserId(organizationModel.getUserId());
		o.setLastNameOfEmpOrAuthorizedRepresentative(organizationModel.getLastNameOfEmpOrAuthorizedRepresentative());
		o.setFirstNameOfEmpOrAuthorizedRepresentative(organizationModel.getFirstNameOfEmpOrAuthorizedRepresentative());
		o.setEmpBusinessOrOrgName(organizationModel.getEmpBusinessOrOrgName());
		o.setTitleOfEmpOrAuthorizedRepresentative(organizationModel.getTitleOfEmpOrAuthorizedRepresentative());
		o.setFirstDayOfEmployment(organizationModel.getFirstDayOfEmployment());
		o.setDateofRehire(organizationModel.getDateofRehire());
		o.setStreetNumberAndName(organizationModel.getStreetNumberAndName());
		o.setPinCodeOrZipCode(organizationModel.getPinCodeOrZipCode());
		o.setTownCity(organizationModel.getTownCity());
		o.setState(organizationModel.getState());
		o.setCountry(organizationModel.getCountry());
		o.setSalary(organizationModel.getSalary());
		o.setClientName(organizationModel.getClientName());
		o.setEmploymentLocation(organizationModel.getEmploymentLocation());
		o.setProjectEndDate(organizationModel.getProjectEndDate());
		return organizationRepository.updateOrganization(o.getLastNameOfEmpOrAuthorizedRepresentative(),
				o.getFirstNameOfEmpOrAuthorizedRepresentative(),
				o.getEmpBusinessOrOrgName(),
				o.getTitleOfEmpOrAuthorizedRepresentative(),
				o.getFirstDayOfEmployment(),
				o.getDateofRehire(),
				o.getStreetNumberAndName(),
				o.getPinCodeOrZipCode(),
				o.getTownCity(),
				o.getState(),
				o.getCountry(),
				o.getSalary(),
				o.getClientName(),
				o.getEmploymentLocation(),
				o.getProjectEndDate(),
				o.getUserId());
	}

	@Override
	public List<State> getStateList(Integer countryId) {
		List<State> stateList = stateRepository.getStateList(countryId);
		return stateList;
	}

	@Override
	public Integer getCountryId(String country) {
		Integer countryId = countryRepository.getCountryId(country);
		return countryId;
	}

	@Override
	public List<Country> getCountryList() {
		List<Country> Country = countryRepository.getCountryList();
		return Country;
	}

	@Override
	public Integer getStateId(String state) {
		Integer stateId = stateRepository.getStateId(state);
		return stateId;
	}

	@Override
	public List<City> getCityList(Integer stateId) {
		List<City> cityList = cityRepository.getCityList(stateId);
		return cityList;
	}

}
