package com.retelzy.employee.service;


import java.util.List;

import com.retelzy.employee.entity.Address;
import com.retelzy.employee.entity.City;
import com.retelzy.employee.entity.Country;
import com.retelzy.employee.entity.Employee;
import com.retelzy.employee.entity.Organization;
import com.retelzy.employee.entity.State;
import com.retelzy.employee.model.AddressModel;
import com.retelzy.employee.model.EmployeeModel;
import com.retelzy.employee.model.OrganizationModel;

public interface EmployeeService {
	
	Employee saveEmployee(EmployeeModel employeeModel);

	Address saveAddress(AddressModel addressModel);

	Organization saveOrganization(OrganizationModel organizationModel);

	Employee getEmployeeInfo(String email);
	
	Address getAddressInfo(String email);
	
	Organization getOrganizationInfo(Integer userId);

	Integer checkRecord(Long userId);

	Integer updateEmployee(EmployeeModel employeeModel);

	Integer checkRecordInAddress(Long userId);

	Integer checkRecordInOrganization(Long userId);

	Integer updateAddress(AddressModel addressModel);

	Integer updateOrganization(OrganizationModel organizationModel);

	List<State> getStateList(Integer country);

	Integer getCountryId(String country);

	List<Country> getCountryList();

	Integer getStateId(String state);

	List<City> getCityList(Integer stateId);

}
