package com.retelzy.employee.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.retelzy.employee.entity.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
	
	@Query(value = "select id from country where name=?1", nativeQuery = true)
	Integer getCountryId(String country);

	@Query(value = "select * from country", nativeQuery = true)
	List<Country> getCountryList();

}