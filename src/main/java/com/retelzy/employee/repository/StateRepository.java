package com.retelzy.employee.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.retelzy.employee.entity.State;

@Repository
public interface StateRepository extends JpaRepository<State, Long> {

	@Query(value = "select * from state where country_id=?1", nativeQuery = true)
	List<State> getStateList(Integer countryId);

	@Query(value = "select id from state where name=?1", nativeQuery = true)
	Integer getStateId(String state);
	
}