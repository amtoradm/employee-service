package com.retelzy.employee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.retelzy.employee.entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
	
	@Query(value = "select * from address where user_id=?1", nativeQuery = true)
	Address getAddress(String email);

	@Query(value = "select * from address where user_id=?1", nativeQuery = true)
	Integer checkRecordInAddress(Long userId);

	@Modifying
    @Transactional
	@Query(value = "update address set apt_number=?1, sreet_number_and_name=?2, pin_code_or_zip_code=?3, area_locality=?4, town_city=?5, state=?6, country=?7 where user_id=?8", nativeQuery = true)
	Integer updateAddress(String aptNumber, String sreetNumberAndName, Double pinCodeOrZipCode, String areaLocality,
			String townCity, String state, String country, Long userId);

}
