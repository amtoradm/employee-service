package com.retelzy.employee.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.retelzy.employee.entity.Organization;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {

	@Query(value = "select * from organization where user_id=?1", nativeQuery = true)
	Organization getOrganization(Integer userId);

	@Query(value = "select * from organization where user_id=?1", nativeQuery = true)
	Integer checkRecordInOrganization(Long userId);

	@Modifying
    @Transactional
	@Query(value = "update organization set last_name_of_emp_or_authorized_representative=?1, first_name_of_emp_or_authorized_representative=?2, emp_business_or_org_name=?3, title_of_emp_or_authorized_representative=?4, first_day_of_employment=?5, dateof_rehire=?6, street_number_and_name=?7, pin_code_or_zip_code=?8, town_city=?9, state=?10, country=?11, salary=?12, client_name=?13, employment_location=?14, project_end_date=?15  where user_id=?16", nativeQuery = true)
	Integer updateOrganization(
			String lastNameOfEmpOrAuthorizedRepresentative,
			String firstNameOfEmpOrAuthorizedRepresentative, 
			String empBusinessOrOrgName,
			String titleOfEmpOrAuthorizedRepresentative, 
			Date firstDayOfEmployment, 
			Date dateofRehire, 
			String streetNumberAndName,
			Double pinCodeOrZipCode, 
			String townCity, 
			String state, 
			String country, 
			String salary, 
			String clientName, 
			String employmentLocation, 
			Date projectEndDate, 
			Long userId);

}
