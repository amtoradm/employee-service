package com.retelzy.employee.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.retelzy.employee.entity.City;
import com.retelzy.employee.entity.State;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

	@Query(value = "select * from city where state_id=?1", nativeQuery = true)
	List<City> getCityList(Integer stateId);
	
}