package com.retelzy.employee.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.retelzy.employee.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	Boolean existsByEmail(String username);

	Employee findByEmail(String email);
	
	@Query(value = "select * from employee where user_id=?1", nativeQuery = true)
	Employee getEmployee(String email);

	@Query(value = "select * from employee where user_id=?1", nativeQuery = true)
	Integer checkEmpRecord(Long userId);

	@Modifying
    @Transactional
	@Query(value = "update employee set middle_initial=?1, other_last_name=?2, date_of_birth=?3, gender=?4, ssn=?5, telephone_number=?6, mobile_number=?7, alternate_mobile_number=?8 where user_id=?9", nativeQuery = true)
	Integer updateEmployee( String middleInitial, String otherLastName,
			Date dateOfBirth, String gender, Double ssn, Double telephoneNumber,
			Double mobileNumber, Double alternateMobileNumber,Long userId);
	
}
