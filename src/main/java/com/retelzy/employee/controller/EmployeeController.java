package com.retelzy.employee.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.retelzy.employee.entity.Address;
import com.retelzy.employee.entity.City;
import com.retelzy.employee.entity.Country;
import com.retelzy.employee.entity.Employee;
import com.retelzy.employee.entity.Organization;
import com.retelzy.employee.entity.State;
import com.retelzy.employee.model.AddressModel;
import com.retelzy.employee.model.EmployeeModel;
import com.retelzy.employee.model.OrganizationModel;
import com.retelzy.employee.repository.EmployeeRepository;
import com.retelzy.employee.response.MessageResponse;
import com.retelzy.employee.service.EmployeeService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Slf4j
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	EmployeeRepository employeeRepository;
	
    @GetMapping("/hello")
    public String hello() {
        return "Welcome in Employee Service";
    }

	@PostMapping("/employee")
	public ResponseEntity<?> saveEmployee(@RequestBody EmployeeModel employeeModel, final HttpServletRequest request) {
		Integer record = employeeService.checkRecord(employeeModel.getUserId());
		if(record != null) {
			employeeService.updateEmployee(employeeModel);
		}

		return ResponseEntity.ok(new MessageResponse("Employee Information saved successfully!"));
	}

	@PostMapping("/checkEmpRecord")
	public ResponseEntity<?> saveInitialEmployeeData(@RequestBody EmployeeModel employeeModel, final HttpServletRequest request) {

		Integer record = employeeService.checkRecord(employeeModel.getUserId());
		if(record==null) {
			employeeService.saveEmployee(employeeModel);
		}
		return ResponseEntity.ok(new MessageResponse("Employee Information saved successfully!"));
	}

	
	@PostMapping("/address")
	public ResponseEntity<?> saveAddress(@RequestBody AddressModel addressModel, final HttpServletRequest request) {
		Integer record = employeeService.checkRecordInAddress(addressModel.getUserId());
		if(record==null) {
			employeeService.saveAddress(addressModel);
		}else {
			employeeService.updateAddress(addressModel);
		}
		return ResponseEntity.ok(new MessageResponse("Address Information saved successfully!"));
	}

	@PostMapping("/organization")
	public ResponseEntity<?> saveOrganization(@RequestBody OrganizationModel organizationModel,
			final HttpServletRequest request) {
		Integer record = employeeService.checkRecordInOrganization(organizationModel.getUserId());
		if(record==null) {
			employeeService.saveOrganization(organizationModel);
		}else {
			employeeService.updateOrganization(organizationModel);
		}
		return ResponseEntity.ok(new MessageResponse("Organization Information saved successfully!"));
	}

	@GetMapping(value = "/getEmployee")
	public ResponseEntity<?> getEmployee(@RequestParam("email") String email) {
		Employee employee = employeeService.getEmployeeInfo(email);
		if (employee != null) {
			return new ResponseEntity<>(employee, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/getAddress")
	public ResponseEntity<?> getAddress(@RequestParam("email") String email) {
		Address address = employeeService.getAddressInfo(email);
		if (address != null) {
			return new ResponseEntity<>(address, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/getOrganization")
	public ResponseEntity<?> getOrganization(@RequestParam("userId") Integer userId) {
		Organization organization = employeeService.getOrganizationInfo(userId);
		if (organization != null) {
			return new ResponseEntity<>(organization, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value = "/getStates")
	public ResponseEntity<?> getStates(@RequestParam("country") String country) {
		Integer countryId = employeeService.getCountryId(country);
		
		if (countryId != null) {
			List<State> stateList = employeeService.getStateList(countryId);
			if(stateList!= null) {
				return new ResponseEntity<>(stateList, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value = "/getCountry")
	public ResponseEntity<?> getCountryList() {
		List<Country> countryList = employeeService.getCountryList();
		if (countryList != null) {
				return new ResponseEntity<>(countryList, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
	}
	
	@GetMapping(value = "/getCities")
	public ResponseEntity<?> getCities(@RequestParam("state") String state) {
		Integer stateId = employeeService.getStateId(state);
		
		if (stateId != null) {
			List<City> cityList = employeeService.getCityList(stateId);
			if(cityList!= null) {
				return new ResponseEntity<>(cityList, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	

}
