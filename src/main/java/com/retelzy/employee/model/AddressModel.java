package com.retelzy.employee.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressModel {
    private Long userId;
    private String aptNumber;
    private String sreetNumberAndName;
    private Double pinCodeOrZipCode;
    private String areaLocality;
    private String townCity;
    private String state;
    private String country;
}
