package com.retelzy.employee.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeModel {
	private Long userId;
    private String lastName;
    private String firstName;
    private String middleInitial;
    private String otherLastName;
    private Date dateOfBirth;
    private String gender;
    private Double ssn;
    private String email;
    private Double telephoneNumber;
    private Double mobileNumber;
    private Double alternateMobileNumber;
}
