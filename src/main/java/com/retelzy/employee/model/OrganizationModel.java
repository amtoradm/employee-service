package com.retelzy.employee.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationModel {
    private Long userId;
	private String lastNameOfEmpOrAuthorizedRepresentative;
    private String firstNameOfEmpOrAuthorizedRepresentative;
    private String empBusinessOrOrgName;
    private String titleOfEmpOrAuthorizedRepresentative;
    private Date firstDayOfEmployment;
    private Date dateofRehire;
    private String streetNumberAndName;
    private Double pinCodeOrZipCode;
    private String townCity;
    private String state;
    private String country;
    private String salary;
    private String clientName;
    private String employmentLocation;
    private Date projectEndDate;
}
